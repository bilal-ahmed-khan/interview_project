import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:interview_assignment/utils/navigation.dart';
import 'package:interview_assignment/view/splash/splash_screen.dart';
import 'package:interview_assignment/view/transaction_details_page.dart';
import 'package:interview_assignment/view/transaction_items.dart';
import 'package:intl/intl.dart';


class API{


  // final f = new DateFormat('dd-MM-yyyy hh:mm a');

  var outputFormat = DateFormat('dd/MM/yyyy');

  StreamController postsController = StreamController();
  Future fetchPost() async {

    final response = await http.get(Uri.parse('https://60e29b749103bd0017b4743f.mockapi.io/api/v1/transactions')
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load post');
    }
  }

  loadPosts() async {
    print("loadPosts");
    fetchPost().then((res) async {
      postsController.add(res);
      // print(postsController);
      return postsController.stream;

    });
  }

  streamFunc() {
    return postsController.stream;
  }

  void onTap(context,String date,String amount,String currencyCode,String type, String iban, String description, String bic){
    AppNavigation.navigateTo(context,  TransactionDetailScreen(date,amount,currencyCode,type,iban,description,bic));
  }


}

