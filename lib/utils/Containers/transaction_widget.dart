import 'package:flutter/material.dart';
import 'package:interview_assignment/API/api.dart';
import 'package:interview_assignment/utils/app_strings.dart';
import 'package:interview_assignment/utils/text/text_widget.dart';

class TransactionContainer extends StatelessWidget {
    String date;
    String amount;
    String currency;
    String transactionType;
    TransactionContainer(this.date,this.amount,this.currency,this.transactionType);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  spreadRadius: 5,
                  blurRadius: 8
              )
            ]
        ),
        height: 150,
        width: MediaQuery.of(context).size.width,

        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15,horizontal: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [

              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextWidget(AppStrings.TRANS_DATE,fontWeight: FontWeight.bold,),
                    TextWidget(AppStrings.TRANS_AMOUNT,fontWeight: FontWeight.bold,),
                    TextWidget(AppStrings.TRANS_CURRENCY,fontWeight: FontWeight.bold,),
                    TextWidget(AppStrings.TRANS_TYPE,fontWeight: FontWeight.bold,),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextWidget(date),
                    TextWidget(amount),
                    TextWidget(currency),
                    TextWidget(transactionType),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
