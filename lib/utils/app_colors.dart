import 'package:flutter/material.dart';

class AppColors
{

  static const Color BLACK_COLOR= Colors.black;
  static const Color WHITE_COLOR= Colors.white;
  static const Color SPLASH_BACK_COLOR= Color(0xffc88f4c);

}