class AppStrings
{
  static const String TRANS_DATE = "Date";
  static const String TRANS_AMOUNT = "Amount";
  static const String TRANS_CURRENCY = "Currency";
  static const String TRANS_TYPE = "TransactionType";
  static const String TRANS_IBAN = "Iban";
  static const String TRANS_DESC = "Description";
  static const String TRANS_BIC = "BIC";
  static const String TRANS_PAGE_NAME = "Transactions";
  static const String TRANS_PAGE_DETAIL = "Transactions Details";

}