
import 'package:flutter/material.dart';


class TextWidget extends StatelessWidget {

  TextWidget(this.text,{this.fontWeight = FontWeight.normal , this.textScale = 1.0,this.textColor = Colors.black,this.textAlign = TextAlign.center});

  final String text;
  final FontWeight fontWeight;
  final double textScale;
  final Color textColor;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(text,
      textScaleFactor: textScale,
      textAlign: textAlign,

      style: TextStyle(
        decoration:  TextDecoration.none,
      fontWeight: fontWeight,
        color: textColor,
height: 1.5


    ),);
  }
}
