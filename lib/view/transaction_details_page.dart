import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:interview_assignment/utils/app_colors.dart';
import 'package:interview_assignment/utils/app_strings.dart';
import 'package:interview_assignment/utils/navigation.dart';
import 'package:interview_assignment/utils/text/text_widget.dart';

class TransactionDetailScreen extends StatelessWidget {

  String date;
  String amount;
  String currency;
  String transactionType;
  String iban;
  String description;
  String bic;
  TransactionDetailScreen(this.date,this.amount,this.currency,this.transactionType, this.iban,this.description,this.bic);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.WHITE_COLOR,
        leading: GestureDetector(
          onTap: (){
            AppNavigation.navigatorPop(context);
          },
          child: Icon(
            Icons.arrow_back_ios_outlined,
            color: AppColors.BLACK_COLOR,
          ),
        ),
        title: TextWidget(
          AppStrings.TRANS_PAGE_DETAIL,
          textScale: 0.9,

        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(

          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      TextWidget(AppStrings.TRANS_DATE,fontWeight: FontWeight.bold,),
                      TextWidget(AppStrings.TRANS_AMOUNT,fontWeight: FontWeight.bold,),
                      TextWidget(AppStrings.TRANS_CURRENCY,fontWeight: FontWeight.bold,),
                      TextWidget(AppStrings.TRANS_TYPE,fontWeight: FontWeight.bold,),
                      TextWidget(AppStrings.TRANS_IBAN,fontWeight: FontWeight.bold,),

                      TextWidget(AppStrings.TRANS_BIC,fontWeight: FontWeight.bold,),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      TextWidget(date),
                      TextWidget(amount),
                      TextWidget(currency),
                      TextWidget(transactionType),
                      TextWidget(iban),
                      TextWidget(bic),


                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextWidget(AppStrings.TRANS_DESC,fontWeight: FontWeight.bold,),
                  TextWidget(description,textAlign: TextAlign.start,),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
