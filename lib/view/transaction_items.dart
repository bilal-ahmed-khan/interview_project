import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:interview_assignment/API/api.dart';
import 'package:interview_assignment/utils/Containers/transaction_widget.dart';
import 'package:interview_assignment/utils/app_strings.dart';
import 'package:interview_assignment/utils/navigation.dart';
import 'package:interview_assignment/utils/text/text_widget.dart';
import 'package:interview_assignment/API/api.dart';
import 'package:http/http.dart' as http;

class TransactionPage extends StatefulWidget {
  const TransactionPage({Key key}) : super(key: key);

  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  API api = API();

  @override
  void initState() {
    api.postsController = new StreamController();
    api.loadPosts();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: TextWidget(
          AppStrings.TRANS_PAGE_NAME,
          textScale: 0.8,
        ),
        centerTitle: true,
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: StreamBuilder(
          stream: api.streamFunc(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Container(
                height: 600,
                width: double.infinity,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }

            return ListView.builder(
              physics: const AlwaysScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                var post = snapshot.data[index];
                return GestureDetector(
                    onTap: () {
                      api.onTap(
                        context,api.outputFormat.format(DateTime.parse(post['date'])),
                        post['amount'],
                        post['currencyCode'],
                        post['type'],
                        post['iban'],
                        post['description'],
                        post['bic'],
                      );
                    },
                    child: TransactionContainer(
                      api.outputFormat.format(DateTime.parse(post['date'])),
                      post['amount'],
                      post['currencyCode'],
                      post['type'],

                    ));
              },
            );
          },
        ),
      ),
    );
  }
}
