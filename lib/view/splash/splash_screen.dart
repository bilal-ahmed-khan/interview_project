import 'dart:async';

import 'package:flutter/material.dart';
import 'package:interview_assignment/utils/app_colors.dart';
import 'package:interview_assignment/utils/app_strings.dart';
import 'package:interview_assignment/utils/asset_paths.dart';
import 'package:interview_assignment/utils/navigation.dart';
import 'package:interview_assignment/utils/text/text_widget.dart';
import 'package:interview_assignment/view/transaction_items.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _splashTimer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        width:MediaQuery.of(context).size.width,
        height:MediaQuery.of(context).size.height,
        color:AppColors.SPLASH_BACK_COLOR,

        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            TextWidget("Interview Assignment",textColor: AppColors.WHITE_COLOR,textScale: 1.8,),
            SizedBox(height:25.0),
            Container(
              width:MediaQuery.of(context).size.width,
              height:MediaQuery.of(context).size.height*0.4,
              decoration: BoxDecoration(
                image:DecorationImage(
                  image:AssetImage(AssetPaths.SPLASH_FOREGROUND_IMAGE),
                  fit:BoxFit.contain
                )
              ),
            ),


          ]
        ),
      )
    );
  }

  Future<Timer> _splashTimer() async {
    return Timer(Duration(seconds: 3), _onComplete);
  }

  void _onComplete(){
    AppNavigation.navigateTo(context,  TransactionPage());
  }





}
